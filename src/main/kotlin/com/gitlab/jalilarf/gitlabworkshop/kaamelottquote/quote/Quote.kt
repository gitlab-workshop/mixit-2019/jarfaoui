package com.gitlab.jalilarf.gitlabworkshop.kaamelottquote.quote

import com.gitlab.jalilarf.gitlabworkshop.kaamelottquote.persona.Persona
import javax.persistence.*


@Entity
class Quote(
        @Id @GeneratedValue val id: Long?,
        @ManyToOne val author: Persona,
        @Column(length = 65555) val body: String
)

