package com.gitlab.jalilarf.gitlabworkshop.kaamelottquote.persona

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Persona(
        @Id @GeneratedValue val id: Long?,
        val firstName: String,
        val lastName: String
)
