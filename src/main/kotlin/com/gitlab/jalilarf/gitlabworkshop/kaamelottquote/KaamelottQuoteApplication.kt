package com.gitlab.jalilarf.gitlabworkshop.kaamelottquote

import com.gitlab.jalilarf.gitlabworkshop.kaamelottquote.persona.Persona
import com.gitlab.jalilarf.gitlabworkshop.kaamelottquote.persona.PersonaRepository
import com.gitlab.jalilarf.gitlabworkshop.kaamelottquote.quote.Quote
import com.gitlab.jalilarf.gitlabworkshop.kaamelottquote.quote.QuoteRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.stereotype.Component

@SpringBootApplication
class KaamelottQuoteApplication

fun main(args: Array<String>) {
    runApplication<KaamelottQuoteApplication>(*args)
}

@Component
class QuoteInitialisation(val quote: QuoteRepository, val persona: PersonaRepository): CommandLineRunner {

    override fun run(vararg args: String?) {
        val arthur = findOrCreate("Arthur", "Pendragon")
        val guenievre = findOrCreate("Guenièvre", "De Carmélide")
        val leodagan = findOrCreate("Leodagan", "De Carmélide")
        val merlin = findOrCreate("Merlin", "🧙‍♂️")
        val perceval = findOrCreate("Perceval", "De Galles")
        val karadok = findOrCreate("Karadok", "De Vannes")
        val bohort = findOrCreate("Bohort", "De Gaunes")
        val ducAquitaine = findOrCreate("Duc", "D'aquitaine")
        val roiBuguonde = findOrCreate("Roi", "Burguonde")
        val dameSeli = findOrCreate("Seli", "De Carmélide")
        val tanteCryda = findOrCreate("Cryda", "De Tintagel")
        val dameDuLac = findOrCreate("La Dame", "Du Lac") // 😅
        val elias = findOrCreate("Élias", "de Kelliwic’h")
        val yvain = findOrCreate("Yvain", "De Carmélide")
        val gauvain = findOrCreate("Gauvain", "d’Orcanie")
        val kadoc = findOrCreate("Kadoc", "De Vannes")
        val caesar = findOrCreate("Cesar", "")

        findOrCreate(arthur, "J'suis roi de Bretagne, j'ai pas de conseil à recevoir d'une clodo !")
        findOrCreate(guenievre, """Mais avant ma vie, c’était de la merde vous entendez?! Recevoir le chef de ci, le roi de mi, toujours polie, toujours bien mise, le symbole de la nation bretonne, ha! Il en faut bien des compensations pour encaisser toute ces conneries hein! Toujours s’occuper de tout, pis surtout de vous, parce que vous avez des responsabilités! Et qui s’occupe de moi pendant c’temps?! Alors oui, maintenant qu'y a plus de pâte d’amande, je tourne en rond, je suis sur les nerfs!! J’ai pas d’amis, pas de loisirs... Comme vous me touchez pas, les choses de l’amour je m’assois dessus, et je parle au figuré, alors je m'suis plongée dans la pâte d'amande! Quand je vous regarde et que j'vois comment vous m'traitez, hein, je m'dis que j’aurais meilleur compte d’aller d'ici jusqu’à Rome à pieds pour en chercher parce que c’est finalement la meilleure chose qui me soit arrivée...""")
        findOrCreate(leodagan, """Ben moi en général, je lui réponds "merde". En principe ça colle avec tout.""")
        findOrCreate(merlin, """Moi, je dis que c'est magique à cause des merdes qui pendouillent, mais ça se trouve, c'est pas ça ...""")
        findOrCreate(perceval, """Si Joseph d'Arimathie a pas été trop con, vous pouvez être sûr que le Graal, c'est un bocal à anchois.""")
        findOrCreate(karadok, "La politique de l'autruche, c'est Une politique qui court vite, une politique qui fait des gros œufs, c'est tout.")
        findOrCreate(bohort, "J'irai me coucher quand vous m'aurez juré qu'il n'y a pas dans cette forêt d'animal plus dangereux que le lapin adulte !")
        findOrCreate(ducAquitaine, """Ah mais non... Mais vous vous êtes encore sur l'ancienne ! […] Tout à fait, excusez-moi, j'ai pas percuté. Non non, l'autre elle est morte. Heu... Les articulations soutenaient plus son poids. Donc elle a commencé par se remplir d'eau par les coudes et après c'est passé aux chevilles, c'est remonté aux genoux et un matin, elle avait tellement gonflé que j'ai appelé la garde. Alors heu... Ils sont venus, ils lui ont mis un coup de lance et puis elle a essayé de se... de se faufiler comme ça, pour se cacher sous le buffet. Pis elle passait pas parce que bon ben... Elle avait plus conscience de sa masse donc elle est restée là, comme ça, pis... Bah six heures après, elle était crevée. Donc, la Duchesse d'Aquitaine ! """)
        findOrCreate(roiBuguonde, "La fleur en bouquet fane, et jamais ne renaît !")
        findOrCreate(dameSeli, """Je vais vous la faire à la cantonade dans le genre tragédie grecque, tâchez de vous partager ça au mieux : LA BOUFFE EST INTERDITE EN DEHORS DES HEURES DES REPAS !!!""")
        findOrCreate(tanteCryda, """Allez, vous devriez mettre les bouts, les demi-sels ! C'est gentil d'être passés ! On va vous faire un p'tit sac avec des restes pour manger chez vous.""")
        findOrCreate(dameDuLac, """Je sais ni boire, ni manger, ni m’habiller, ni me laver, ni rien ! Vous sentez que je vais être un fardeau pour vous, ou pas ?""")
        findOrCreate(elias, """Mais retournez à l’état sauvage, espèce de con. Foutez le camp dans les montagnes avec une paire de chèvres. On vous achètera vos fromages, promis. […] Vous le sentez pas, l'appel de la nature, là? Les animaux de la forêt, y z'ont pas besoin d'un toubib ? […] Tirez-vous d'ici. Définitivement. Et laissez bosser ceux qui savent faire ! Y a pas de place pour les amateurs ici.""")
        findOrCreate(yvain, """Est-ce qu'on peut s'en servir pour donner de l'élan à un pigeon ?""")
        findOrCreate(gauvain, """Nous sommes jeunes, nous marchons à pieds… J’opterai donc pour un surnom en rapport : "les Petits Pédestres".""")
        findOrCreate(kadoc, """J'ai le droit d'être 4 jours pas chez moi, et après chez moi. Mais y a du voyage qui se prépare, et pour soigner les bêtes, y a pas que ma tante, y a moi aussi.""")
        findOrCreate(caesar, """Des gamins de partout, plein la chambre. Je sais pas... soixante, quatre-vingt. Des tout petits, de quatre ou cinq ans, et tous ensemble : "Ave votre tranquillité", alors moi comme un con : "Ave les enfants". Alors, ça vous a plu, la visite du palais ? Qu'est-ce que vous avez vu de beau ? Euh, bref. Je raconte mes conneries habituelles, puis tout d'un coup j'en repère un, sur le devant, un petit mec, avec des mèches en pétard, et un petit paquet dans la main. On aurait dit que, il faisait la gueule. Comment tu t'appelles ? Ptt, pas de réponse. Il est drôlement joli ton paquet. Oh, ni oui ni merde. Tu veux pas me dire ce que c'est ? "C'est un cadeau pour le général", qu'y me fait. Bah vous me croirez ou non, j'ai eu beau lui dire que c'était moi le général, y a pas eu moyen. Alors je l'ai pris tout seul avec moi, ça m'a pris la journée. J'ai montré mon uniforme, je l'ai emmené dans la salle des cartes, j'ai montré des maquettes de bateaux. Un moment il faisait presque nuit, je lui ai dit "écoute, ça va peut-être aller là, non ? Bah, tu vois quand même bien que c'est moi le général, hein, alors tu me donne le paquet pis on n'en parle plus". Il m'a dit "d'accord". C'était des petites meringues, blanches, rondes comme ça. Ah, drôlement bonnes. On les a mangées tous les deux sur la terrasse, sans rien dire. Voilà. Si je devais choisir une journée à revivre, je prendrais celle-là.""")
    }

    fun findOrCreate(firstName: String, lastName: String) =
            persona.findFirstByFirstNameAndLastName(firstName, lastName) ?: persona.save(Persona(null, firstName, lastName))

    fun findOrCreate(author: Persona, body: String) {
        quote.findByBody(body) ?: quote.save(Quote(null, author, body))
    }
}
